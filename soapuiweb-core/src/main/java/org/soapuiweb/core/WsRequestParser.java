package org.soapuiweb.core;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.soapuiweb.core.EnumConsts.DataType;
import org.soapuiweb.core.exception.WsRequestCreationException;
import org.soapuiweb.core.model.AttributeMeta;
import org.soapuiweb.core.model.ElementMeta;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.predic8.soamodel.Difference;
import com.predic8.wsdl.Definitions;
import com.predic8.wsdl.WSDLParser;
import com.predic8.wsdl.diff.WsdlDiffGenerator;

public class WsRequestParser implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id = 1;

	private static final String ID_ATTRIBUTE = "tempIdAttr";

	private Document document;

	private ElementMeta rootMeta;

	private Map<Long, ElementMeta> metaList = new HashMap<Long, ElementMeta>();;

	private static final Pattern manyPatern = Pattern.compile(".* from [\\w]+ to .*");

	private static final Log logger = LogFactory.getLog(WsRequestParser.class);

	public static void main(String[] args) {
		compare();
	}

	private static void compare() {
		WSDLParser parser = new WSDLParser();

		Definitions wsdl1 = parser.parse("https://www.sbm.org.tr/TrafikPoliceWS-prod-V2.0/TrafikPoliceIslemleriServisiSoapHttpPort?wsdl");

		Definitions wsdl2 = parser.parse("https://www.sbm.org.tr/TrafikPoliceWS-test-V2.0/TrafikPoliceIslemleriServisiSoapHttpPort?wsdl");

		WsdlDiffGenerator diffGen = new WsdlDiffGenerator(wsdl1, wsdl2);
		List<Difference> lst = diffGen.compare();
		for (Difference diff : lst) {
			dumpDiff(diff, "");
		}
	}

	private static void dumpDiff(Difference diff, String level) {
		System.out.println(level + diff.getDescription());
		for (Difference localDiff : diff.getDiffs()) {
			dumpDiff(localDiff, level + "  ");
		}
	}

	public ElementMeta parse(String xml) throws WsRequestCreationException {
		try {
			document = Utils.toDocument(xml);

			setIds(document);
			setRelatios(document);
			removeIds(document);

			debug(rootMeta);
			return rootMeta;
		} catch (Exception e) {
			logger.error("", e);
			throw new WsRequestCreationException(e);
		}
	}

	public String toXml(ElementMeta me) {
		StringWriter writer = new StringWriter();
		toXml(me, writer);
		return writer.toString();
	}

	public void toXml(ElementMeta root, StringWriter writer) {
		writer.append("<").append(root.getName());
		if (root.getAttributes() != null && root.getAttributes().size() > 0) {
			writer.append(" ");

			for (int i = 0; i < root.getAttributes().size(); i++) {
				AttributeMeta at = root.getAttributes().get(i);
				writer.append(at.getName()).append("=\"").append(nullControlled(at.getValue())).append("\"");
				if (i < root.getAttributes().size() - 1) {
					writer.append(" ");
				}
			}
		}
		writer.append(">");
		if (root.getValue() != null) {
			writer.append(root.getValue());
		}
		if (root.getChilds() != null) {
			for (ElementMeta child : root.getChilds()) {
				toXml(child, writer);
			}
		}
		writer.append("</").append(root.getName()).append(">");
	}

	private static String nullControlled(String val) {
		return val == null ? "" : val;//StringEscapeUtils.escapeJava(val);
	}

	private void debug(ElementMeta mc) {
		writeS(mc);
		String cm = mc.getComments() == null ? "" : " c:" + mc.getComments();
		String dt = mc.getDataType() == null ? "" : " dt:" + mc.getDataType();
		String pv = mc.getPossibleValues() == null ? "" : " pv:" + mc.getPossibleValues();
		String many = mc.getMany() == null ? "" : " many:" + mc.getMany();
		String attr = mc.getAttributes() == null ? "" : " attr:" + mc.getAttributes();
		// System.out.println(mc.getDomElement().toString() + cm + dt + pv +
		// many + attr);
		logger.debug(mc.getName() + cm + dt + pv + many + attr);
		if (mc.getChilds() != null) {
			for (ElementMeta c : mc.getChilds()) {
				debug(c);
			}
		}
	}

	private void writeS(ElementMeta e) {
		while (e.getParent() != null) {
			System.out.print(" ");
			e = e.getParent();
		}
	}

	private void setIds(Node node) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			Element e = (Element) node;
			e.setAttribute(ID_ATTRIBUTE, "" + id++);
		}
		NodeList childNodes = node.getChildNodes();
		if (childNodes != null && childNodes.getLength() > 0) {
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node item = childNodes.item(i);
				setIds(item);
			}
		}
	}

	private void removeIds(Node node) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			Element e = (Element) node;
			e.removeAttribute(ID_ATTRIBUTE);
		}
		NodeList childNodes = node.getChildNodes();
		if (childNodes != null && childNodes.getLength() > 0) {
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node item = childNodes.item(i);
				removeIds(item);
			}
		}
	}

	private void setRelatios(Node node) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			Element e = (Element) node;
			ElementMeta mc = createMetaClass(e);
			mc.setParent(createMetaClass(getParentElement(e)));
			if (mc.getParent() != null) {
				mc.getParent().appendChild(mc);
			}
			mc.setPrevious(createMetaClass(getPreviousElement(e)));
			mc.setNext(createMetaClass(getNextElement(e)));
		}

		NodeList childNodes = node.getChildNodes();
		if (childNodes != null && childNodes.getLength() > 0) {
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node item = childNodes.item(i);
				setRelatios(item);
			}
		}
	}

	public ElementMeta clone(ElementMeta node) {
		_clone(node);
		return rootMeta;
	}

	private void _clone(ElementMeta node) {
		ElementMeta mc = createMetaClass(node);
		if(mc != rootMeta) {
			mc.setParent(createMetaClass(node.getParent()));
			if (mc.getParent() != null) {
				mc.getParent().appendChild(mc);
			}
			mc.setPrevious(createMetaClass(node.getPrevious()));
			mc.setNext(createMetaClass(node.getNext()));
		}

		List<ElementMeta> childNodes = node.getChilds();
		if (childNodes != null && childNodes.size() > 0) {
			for (int i = 0; i < childNodes.size(); i++) {
				ElementMeta item = childNodes.get(i);
				_clone(item);
			}
		}
	}

	private ElementMeta createMetaClass(Element e) {
		if (e == null) {
			return null;
		}
		long id = Long.parseLong(e.getAttribute(ID_ATTRIBUTE));
		ElementMeta mc = metaList.get(id);
		if (mc == null) {
			mc = new ElementMeta();
			mc.setId(id);
			mc.setName(e.getTagName());
			setComment(mc, e);
			setDataType(mc, e);
			setCommentData(mc);
			setAttributeList(mc, e);
			if (id == 1) {
				rootMeta = mc;
			}
			metaList.put(id, mc);
		}
		return mc;
	}

	private ElementMeta createMetaClass(ElementMeta e) {
		if (e == null) {
			return null;
		}
		long id = e.getId();
		ElementMeta mc = metaList.get(id);
		if (mc == null) {
			mc = new ElementMeta();
			mc.setId(id);
			mc.setName(e.getName());
			mc.setComments(e.getComments());
			mc.setPossibleValues(e.getPossibleValues());
			mc.setDataType(e.getDataType());
			mc.setMany(e.getMany());
			mc.setOptional(e.getOptional());
			mc.setValue(e.getValue());

			if (e.getAttributes() != null) {
				mc.setAttributes(new ArrayList<AttributeMeta>());
				for (AttributeMeta a : e.getAttributes()) {
					AttributeMeta m = new AttributeMeta();
					m.setDataType(a.getDataType());
					m.setName(a.getName());
					m.setValue(a.getValue());
					m.setParent(mc);
					mc.getAttributes().add(m);
				}
			}
			if (rootMeta == null) {
				rootMeta = mc;
			}
			metaList.put(id, mc);
		}
		return mc;
	}

	private void setComment(ElementMeta mc, Element domElement) {
		List<Comment> cList = new ArrayList<Comment>();
		Node element = domElement.getPreviousSibling();
		while (element != null) {
			if (element.getNodeType() == Node.COMMENT_NODE) {
				cList.add((Comment) element);
			} else if (element.getNodeType() == Node.ELEMENT_NODE) {
				break;
			}
			element = element.getPreviousSibling();
		}
		if (cList.size() != 0) {
			for (Comment comment : cList) {
				if (mc.getComments() == null) {
					mc.setComments(new ArrayList<String>());
				}
				if (!mc.getComments().contains(comment.getTextContent())) {
					mc.getComments().add(comment.getTextContent());
				}
			}
		}
	}

	private void setDataType(ElementMeta mc, Element domElement) {
		if (domElement.getChildNodes().getLength() == 1 && domElement.getChildNodes().item(0).getNodeType() == Node.TEXT_NODE) {
			String def = domElement.getChildNodes().item(0).getNodeValue();
			if (def != null && def.length() > 0) {
				mc.setDataType(getDataType(def));
			}
		}
	}

	private DataType getDataType(String def) {
		if (def.equals("?999?")) {
			return DataType.LONG;
		} else if (def.equals("?XXX?")) {
			return DataType.STRING;
		} else if (def.equals("?-999?")) {
			return DataType.LONG;
		} else if (def.equals("?999.99?")) {
			return DataType.DECIMAL;
		} else if (def.equals("???")) {
			return DataType.STRING;
		} else if (def.equals("2008-12-31T23:59:00.000-05:00")) {
			return DataType.DATETIME;
		} else if (def.equals("2008-12-31")) {
			return DataType.DATE;
		} else if (def.equals("?true?")) {
			return DataType.BOOLEAN;
		}
		return null;
	}

	private void setAttributeList(ElementMeta mc, Element domElement) {
		if (domElement.getAttributes() != null) {
			mc.setAttributes(new ArrayList<AttributeMeta>());
			NamedNodeMap attrs = domElement.getAttributes();
			for (int i = 0; i < attrs.getLength(); i++) {
				Node item = attrs.item(i);
				if (ID_ATTRIBUTE.equals(item.getNodeName())) {
					continue;
				}
				String def = item.getNodeValue();
				DataType dataType = null;
				if (def != null && def.length() > 0) {
					dataType = getDataType(def);
				}
				AttributeMeta attr = new AttributeMeta();
				attr.setName(item.getNodeName());
				attr.setDataType(dataType);
				attr.setParent(mc);
				attr.setValue(item.getNodeValue());
				mc.getAttributes().add(attr);
			}
			if (mc.getAttributes().size() == 0) {
				mc.setAttributes(null);
			}
		}
	}

	private void setCommentData(ElementMeta mc) {
		if (mc.getComments() != null && mc.getComments().size() > 0) {
			for (String comment : mc.getComments()) {
				mc.setOptional(comment.contains("optional"));
				mc.setMany(manyPatern.matcher(comment).matches());
				if (comment.contains("possible value:")) {
					String[] ss1 = comment.split(",");
					for (String s1 : ss1) {
						String[] ss2 = s1.split("possible value:");
						if (ss2.length == 2) {
							if (mc.getPossibleValues() == null) {
								mc.setPossibleValues(new ArrayList<String>());
								mc.setDataType(DataType.ENUM);
							}
							mc.getPossibleValues().add(ss2[1].trim());
						}
					}
				}
			}
		}
	}

	private Element getParentElement(Node e) {
		while (e.getParentNode() != null) {
			if (e.getParentNode().getNodeType() == Node.ELEMENT_NODE) {
				return (Element) e.getParentNode();
			}
			return getParentElement(e.getParentNode());
		}
		return null;
	}

	private Element getPreviousElement(Node e) {
		while (e.getPreviousSibling() != null) {
			if (e.getPreviousSibling().getNodeType() == Node.ELEMENT_NODE) {
				return (Element) e.getPreviousSibling();
			}
			return getPreviousElement(e.getPreviousSibling());
		}
		return null;
	}

	private Element getNextElement(Node e) {
		while (e.getNextSibling() != null) {
			if (e.getNextSibling().getNodeType() == Node.ELEMENT_NODE) {
				return (Element) e.getNextSibling();
			}
			return getNextElement(e.getNextSibling());
		}
		return null;
	}
}
