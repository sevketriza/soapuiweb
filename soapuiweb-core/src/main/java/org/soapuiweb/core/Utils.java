package org.soapuiweb.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Utils {

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);

	public static <T extends Serializable> List<T> clone(List<T> list) {
		List<T> cloneList = new ArrayList<T>(list.size());
		for (T t : list) {
			cloneList.add(clone(t));
		}
		return cloneList;
	}

	public static boolean isValidEmail(final String email) {
		return emailPattern.matcher(email).matches();

	}

	@SuppressWarnings("unchecked")
	public static <T extends Serializable> T clone(T t) {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new ByteArrayInputStream(serialize(t)));
			return (T) ois.readObject();
		} catch (ClassNotFoundException cnfe) {
			throw new RuntimeException(cnfe);
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		} finally {
			if (ois != null) {
				try {
					ois.close();
				} catch (IOException ioe) {
				}
			}
		}
	}

	public static byte[] serialize(Serializable t) throws IOException {
		ObjectOutputStream oos = null;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(bos);
			oos.writeObject(t);
			return bos.toByteArray();
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (IOException ioe) {
				}
			}
		}
	}

	public static Serializable deserialize(byte[] t) throws IOException {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new ByteArrayInputStream(t));
			return (Serializable) ois.readObject();
		} catch (ClassNotFoundException cnfe) {
			throw new RuntimeException(cnfe);
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		} finally {
			if (ois != null) {
				try {
					ois.close();
				} catch (IOException ioe) {
				}
			}
		}
	}

	public static String formatXml(String xml) throws TransformerException, SAXException, ParserConfigurationException, IOException {
		return formatXml(toDocument(xml));
	}

	public static String formatDate(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	public static String formatXml(Document document) throws TransformerException {
		Transformer xformer = TransformerFactory.newInstance().newTransformer();
		xformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		xformer.setOutputProperty(OutputKeys.INDENT, "yes");
		xformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		xformer.setOutputProperty(OutputKeys.METHOD, "xml");
		xformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		xformer.transform(new DOMSource(document), result);
		return writer.toString();
	}

	public static Document toDocument(String xml) throws SAXException, ParserConfigurationException, IOException {
		ByteArrayInputStream bis = new ByteArrayInputStream(xml.getBytes("UTF-8"));
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setValidating(false);
		return dbf.newDocumentBuilder().parse(bis);

	}
}
