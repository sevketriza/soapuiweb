package org.soapuiweb.core;
public class EnumConsts {

	public enum DataType {
		LONG,
		DATE,
		DATETIME,
		DECIMAL,
		ENUM,
		STRING,
		BOOLEAN
	}
}
