package org.soapuiweb.core.exception;

public class WsdlParseException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WsdlParseException(Throwable cause) {
		super(cause);
	}
}

