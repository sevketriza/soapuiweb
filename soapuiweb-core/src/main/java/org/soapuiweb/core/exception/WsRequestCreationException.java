package org.soapuiweb.core.exception;

public class WsRequestCreationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WsRequestCreationException(Throwable cause) {
		super(cause);
	}
}
