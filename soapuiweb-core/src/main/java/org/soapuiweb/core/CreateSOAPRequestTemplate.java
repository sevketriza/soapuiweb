package org.soapuiweb.core;

import groovy.xml.MarkupBuilder;

import java.io.StringWriter;

import com.predic8.wsdl.Binding;
import com.predic8.wsdl.BindingOperation;
import com.predic8.wsdl.Definitions;
import com.predic8.wsdl.WSDLParser;
import com.predic8.wstool.creator.RequestTemplateCreator;
import com.predic8.wstool.creator.SOARequestCreator;

public class CreateSOAPRequestTemplate {

	public static void main(String[] args) {

		WSDLParser parser = new WSDLParser();

		Definitions wsdl = parser.parse("https://www.sbm.org.tr/EgmSorguService-prod-V1.0/egmSorguService?wsdl");

		StringWriter writer = new StringWriter();

		// SOAPRequestCreator constructor: SOARequestCreator(Definitions, Creator, MarkupBuilder)
		SOARequestCreator creator = new SOARequestCreator(wsdl, new RequestTemplateCreator(), new MarkupBuilder(writer));

		// creator.createRequest(PortType name, Operation name, Binding name);
		for (Binding binding : wsdl.getBindings()) {
			for (BindingOperation bo : binding.getOperations()) {
				creator.createRequest(binding.getPortType().getName(), bo.getName(), binding.getName());
			}
		}

		System.out.println(writer);
	}
}