package org.soapuiweb.core.model;

import java.io.Serializable;

import com.predic8.wsdl.BindingOperation;

public class OperationWrapper implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ElementMeta meta;
	private String name;
	private String soapAction;

	public OperationWrapper(BindingOperation operation) {
		this.name = operation.getName();
		this.soapAction = operation.getOperation().getSoapAction();
	}

	public ElementMeta getMeta() {
		return meta;
	}

	public void setMeta(ElementMeta meta) {
		this.meta = meta;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}
}
