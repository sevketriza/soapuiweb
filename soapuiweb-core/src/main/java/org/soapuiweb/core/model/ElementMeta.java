package org.soapuiweb.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.soapuiweb.core.EnumConsts.DataType;
import org.soapuiweb.core.WsRequestParser;

public class ElementMeta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;

	private List<ElementMeta> childs;
	private ElementMeta previous;
	private ElementMeta next;
	private ElementMeta parent;
	private List<AttributeMeta> attributes;

	private String value;
	private String name;

	private DataType dataType;
	private List<String> comments;
	private List<String> possibleValues;
	private Boolean optional;
	private Boolean many;

	public List<ElementMeta> getChilds() {
		return childs;
	}

	public void setChilds(List<ElementMeta> childs) {
		this.childs = childs;
	}

	public ElementMeta getPrevious() {
		return previous;
	}

	public void setPrevious(ElementMeta previous) {
		this.previous = previous;
	}

	public ElementMeta getNext() {
		return next;
	}

	public void setNext(ElementMeta next) {
		this.next = next;
	}

	public ElementMeta getParent() {
		return parent;
	}

	public void setParent(ElementMeta parent) {
		this.parent = parent;
	}

	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return super.hashCode();// Long.valueOf(id).hashCode();
	}

	public void appendChild(ElementMeta child) {
		if (this.childs == null) {
			this.childs = new ArrayList<ElementMeta>();
		}
		if (childs.size() == 0) {
			child.setParent(this);
			childs.add(child);
			child.setNext(null);
			child.setPrevious(null);
		} else {
			ElementMeta last = childs.get(childs.size() - 1);
			last.setNext(child);
			child.setPrevious(last);
			child.setParent(this);
			childs.add(child);
		}
	}

	@Override
	public String toString() {
		return name;
	}

	public List<String> getPossibleValues() {
		return possibleValues;
	}

	public void setPossibleValues(List<String> possibleValues) {
		this.possibleValues = possibleValues;
	}

	public Boolean getOptional() {
		return optional;
	}

	public void setOptional(Boolean optional) {
		this.optional = optional;
	}

	public Boolean getMany() {
		return many;
	}

	public void setMany(Boolean many) {
		this.many = many;
	}

	public List<String> getComments() {
		return comments;
	}

	public void setComments(List<String> comments) {
		this.comments = comments;
	}

	public List<AttributeMeta> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<AttributeMeta> attributes) {
		this.attributes = attributes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toXml() {
		return new WsRequestParser().toXml(this);
	}

	public ElementMeta clone() {
		return new WsRequestParser().clone(this);
	}

	public void insertBefore(ElementMeta newChild, ElementMeta oldChild) {
		newChild.setNext(oldChild);
		newChild.setPrevious(oldChild.getPrevious());
		oldChild.setPrevious(newChild);
		newChild.setParent(oldChild.getParent());
		getChilds().add(getChilds().indexOf(oldChild), newChild);
	}

	public void removeChild(ElementMeta child) {
		ElementMeta next = child.getNext();
		ElementMeta previous = child.getPrevious();
		if (previous != null) {
			previous.setNext(next);
		}
		if (next != null) {
			next.setPrevious(previous);
		}
		getChilds().remove(child);
	}

}
