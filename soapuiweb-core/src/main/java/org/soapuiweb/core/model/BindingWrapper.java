package org.soapuiweb.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.predic8.wsdl.Binding;

public class BindingWrapper implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<OperationWrapper> operations = new ArrayList<OperationWrapper>();

	private String name;

	private String location;

	private String soapVersion;

	public BindingWrapper(Binding binding) {
		super();
		this.name = binding.getName();
		soapVersion = binding.getBinding().getProtocol();
	}

	public List<OperationWrapper> getOperations() {
		return operations;
	}

	public void setOperations(List<OperationWrapper> operations) {
		this.operations = operations;
	}


	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSoapVersion() {
		return soapVersion;
	}

	public void setSoapVersion(String soapVersion) {
		this.soapVersion = soapVersion;
	}

	public String getName() {
		return name;
	}

	public void setName(String bindingName) {
		this.name = bindingName;
	}
}
