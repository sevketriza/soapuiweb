package org.soapuiweb.core.model;

import java.io.Serializable;

import org.soapuiweb.core.EnumConsts.DataType;

public class AttributeMeta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String value;
	private String name;
	private DataType dataType;
	private ElementMeta parent;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	public ElementMeta getParent() {
		return parent;
	}

	public void setParent(ElementMeta parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		return name + " dt:" + dataType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
