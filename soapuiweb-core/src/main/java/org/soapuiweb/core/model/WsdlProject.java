package org.soapuiweb.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WsdlProject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	
	private List<BindingWrapper> bindings = new ArrayList<BindingWrapper>();

	public List<BindingWrapper> getBindings() {
		return bindings;
	}

	public void setBindings(List<BindingWrapper> bindings) {
		this.bindings = bindings;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
