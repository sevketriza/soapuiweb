package org.soapuiweb.core;

import groovy.xml.MarkupBuilder;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.logging.Logger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.soapuiweb.core.exception.WsRequestCreationException;
import org.soapuiweb.core.model.BindingWrapper;
import org.soapuiweb.core.model.ElementMeta;
import org.soapuiweb.core.model.OperationWrapper;
import org.soapuiweb.core.model.WsdlProject;

import com.predic8.wsdl.Binding;
import com.predic8.wsdl.BindingElement;
import com.predic8.wsdl.BindingOperation;
import com.predic8.wsdl.Definitions;
import com.predic8.wsdl.Port;
import com.predic8.wsdl.Service;
import com.predic8.wsdl.WSDLParser;
import com.predic8.wstool.creator.RequestTemplateCreator;
import com.predic8.wstool.creator.SOARequestCreator;

public class WsdlProjectCreator {
	private static WsdlProjectCreator instance;

	private static final Log logger = LogFactory.getLog(WsdlProjectCreator.class);

	private WsdlProjectCreator() {
	}

	static public WsdlProjectCreator getInstance() {
		if (instance != null) {
			return instance;
		}
		return instance = new WsdlProjectCreator();
	}

	public WsdlProject createProject(InputStream in) throws WsRequestCreationException {
		return createProject(new WSDLParser().parse(in));
	}

	public WsdlProject createProject(String wsdlXml) throws WsRequestCreationException {
		return createProject(new WSDLParser().parse(wsdlXml));
	}

	private void setLocation(Definitions wsdl, BindingWrapper bw) {
		if (wsdl.getLocalServices() != null) {
			for (Service s : wsdl.getLocalServices()) {
				if (s.getPorts() != null) {
					for (Port p : s.getPorts()) {
						if (p.getBinding() != null && p.getBinding().getName() != null && p.getAddress() != null && p.getAddress().getLocation() != null
								&& p.getBinding().getName().equals(bw.getName())) {
							bw.setLocation(p.getAddress().getLocation());
						}
					}
				}
			}
		}
	}

	private String createRequestTemplate(Definitions wsdl, Binding binding, BindingOperation bo) throws Exception {
		// for (BindingElement be : bo.getInput().getBindingElements()) {
		// if (be instanceof com.predic8.wsdl.AbstractSOAPBody) {
		// com.predic8.wsdl.AbstractSOAPBody body = (com.predic8.wsdl.AbstractSOAPBody) be;
		// if (body.getParts() == null || body.getParts().size() == 0) {
		// if (bo.getBinding().getProtocol().equals("SOAP11")) {
		// return "<s11:Envelope xmlns:s11=\"http://schemas.xmlsoap.org/soap/envelope/\"><s11:Body/></s11:Envelope>";
		// }
		// if (bo.getBinding().getProtocol().equals("SOAP12")) {
		// return "<s12:Envelope xmlns:s12=\"http://www.w3.org/2003/05/soap-envelope\"><s12:Body/></s12:Envelope>";
		// }
		// }
		//
		// }
		// }
		try {
			StringWriter writer = new StringWriter();
			SOARequestCreator creator = new SOARequestCreator(wsdl, new RequestTemplateCreator(), new MarkupBuilder(writer));
			creator.createRequest(binding.getPortType().getName(), bo.getName(), binding.getName());
			return writer.toString();
		} catch (Exception e) {
			if (bo.getBinding().getProtocol().equals("SOAP11")) {
				return "<s11:Envelope xmlns:s11=\"http://schemas.xmlsoap.org/soap/envelope/\"><s11:Body/></s11:Envelope>";
			} else if (bo.getBinding().getProtocol().equals("SOAP12")) {
				return "<s12:Envelope xmlns:s12=\"http://www.w3.org/2003/05/soap-envelope\"><s12:Body/></s12:Envelope>";
			}
			throw e;
		}
	}

	private WsdlProject createProject(Definitions wsdl) throws WsRequestCreationException {
		WsdlProject project = new WsdlProject();
		project.setName(wsdl.getName());
		for (Binding binding : wsdl.getBindings()) {
			try {
				BindingWrapper bw = new BindingWrapper(binding);
				for (BindingOperation bo : binding.getOperations()) {
					try {
						ElementMeta elementMeta = new WsRequestParser().parse(createRequestTemplate(wsdl, binding, bo));
						OperationWrapper ow = new OperationWrapper(bo);
						ow.setMeta(elementMeta);
						bw.getOperations().add(ow);
					} catch (Exception e) {
						logger.warn("", e);
					}
				}
				setLocation(wsdl, bw);
				project.getBindings().add(bw);
			} catch (Exception e) {
				logger.warn("", e);
			}
		}
		return project;
	}
}
