function resizeDiv() {
	var vpw = $(window).width();
	// vph = $(window).height();
	var w = Math.round(vpw / 4 * 3) - 90;
	$('.pre').css({
		"width" : w + "",
		"display" : "block"
	});
}

function toggleClass() {
	if($.browser.mozilla) {
		return;
	}
	var rm = $("span.ui-treenode-leaf-icon");
	for (var i = 0; i < rm.size(); i++) {
		var e = $(rm[i]);
		var parent = e.parent();
		if (parent.parent().hasClass('requestNode') && parent.hasClass('ui-treenode-content')) {//.hasClass('requestNode')
			parent.removeClass("ui-treenode-content");
		}
	}
}

function appletCompleted() {
}
function onAppletCallComplete() {
}
function createApplet() {
	var applet = document.createElement("applet");
	applet.setAttribute("archive", "applet.jar");
	applet.setAttribute("code", "com.soapuiweb.applet.WSCallerApplet.class");
	applet.setAttribute("width", "0");
	applet.setAttribute("height", "0");
	applet.setAttribute("id", "wsCallerApplet");

	var appletDiv = document.getElementById("wsCallerAppletDiv");

	for (var i = 0; i < appletDiv.childNodes.length; i++) {
		appletDiv.removeChild(appletDiv.childNodes.item(0));
	}

	appletDiv.appendChild(applet);
}

function showLoading() {
	PF('ajaxLoadingPanel').show();
}
function hideLoading() {
	PF('ajaxLoadingPanel').hide();
}