package com.soapuiweb.portal.model;

import java.io.Serializable;
import java.util.List;

import org.primefaces.model.DefaultTreeNode;

public class WsdlProjectWrapper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private DefaultTreeNode root;
	private String wsdl;
	private List<NodeElement> requests;
	private int activeRequestTabIndex;

	public String getWsdl() {
		return wsdl;
	}

	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}

	public List<NodeElement> getRequests() {
		return requests;
	}

	public void setRequests(List<NodeElement> requests) {
		this.requests = requests;
	}

	public DefaultTreeNode getRoot() {
		return root;
	}

	public void setRoot(DefaultTreeNode root) {
		this.root = root;
	}

	public int getActiveRequestTabIndex() {
		return activeRequestTabIndex;
	}

	public void setActiveRequestTabIndex(int activeRequestTabIndex) {
		this.activeRequestTabIndex = activeRequestTabIndex;
	}

}
