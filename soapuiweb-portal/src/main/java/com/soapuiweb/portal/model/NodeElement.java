package com.soapuiweb.portal.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.primefaces.model.TreeNode;
import org.soapuiweb.core.model.ElementMeta;

import com.soapuiweb.portal.model.EnumTypes.NodeType;

public class NodeElement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	private String xml;
	private NodeType nodeType;
	private ElementMeta meta;
	private TreeNode treeNode;
	private TreeNode metaRoot;
	private int activeTab;

	private String location;
	private String soapAction;
	private String soapVersion;
	private String responseXml;

	private List<KeyValue> securityHeaders = new ArrayList<KeyValue>();

	{
		securityHeaders.add(new KeyValue("Username", ""));
		securityHeaders.add(new KeyValue("KurumKod", ""));
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ElementMeta getMeta() {
		return meta;
	}

	public void setMeta(ElementMeta meta) {
		this.meta = meta;
	}

	public NodeType getNodeType() {
		return nodeType;
	}

	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}

	public TreeNode getTreeNode() {
		return treeNode;
	}

	public void setTreeNode(TreeNode treeNode) {
		this.treeNode = treeNode;
	}

	public TreeNode getMetaRoot() {
		return metaRoot;
	}

	public void setMetaRoot(TreeNode metaNode) {
		this.metaRoot = metaNode;
	}

	public String getXml() {
		return xml;
	}

	public String getEscapedXml() {
		return xml == null ? "" : StringEscapeUtils.escapeJavaScript(xml);
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public int getActiveTab() {
		return activeTab;
	}

	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	public String getResponseXml() {
		return responseXml;
	}

	public void setResponseXml(String responseXml) {
		this.responseXml = responseXml;
	}

	public String getSoapVersion() {
		return soapVersion;
	}

	public void setSoapVersion(String soapVersion) {
		this.soapVersion = soapVersion;
	}

	public List<KeyValue> getSecurityHeaders() {
		return securityHeaders;
	}

	public void setSecurityHeaders(List<KeyValue> securityHeaders) {
		this.securityHeaders = securityHeaders;
	}

}
