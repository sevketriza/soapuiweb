package com.soapuiweb.portal.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "dateTimeConverter")
public class DateTimeConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String val) {
		if (val == null || val.isEmpty()) {
			return null;
		}
		if (val.length() >= 11 && Character.isSpaceChar(val.charAt(10))) {
			return val.substring(0, 10) + "T" + val.substring(11);
		}
		return val;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object obj) {
		String val = (String) obj;
		if (val == null || val.isEmpty()) {
			return null;
		}
		if (val.length() >= 11 && val.charAt(10) == 'T') {
			return val.substring(0, 10) + " " + val.substring(11);
		}
		return val;
	}

}
