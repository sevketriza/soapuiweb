package com.soapuiweb.portal.action;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.soapuiweb.portal.model.Project;
import com.soapuiweb.portal.model.User;

@SessionScoped
@ManagedBean(name = "loginHome")
public class LoginHome implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private User user;
	private List<Project> projects;

	private String userName;
	private String password;

	@ManagedProperty(value = "#{authenticator}")
	private Authenticator authenticator;

	public void login() {
		try {
			user = authenticator.login(userName, password);
			RequestContext.getCurrentInstance().addCallbackParam("loggedIn", true);
			loadProjects();
		} catch (Exception e) {
			addErrMessage(e.getMessage());
		}
	}

	public void loadProjects() {
		projects = null;
		try {
			projects = authenticator.getProjectsWithoutData(user).getProjects();
		} catch (Exception e) {
			e.printStackTrace();
			addErrMessage("Projeler y�klenirken hata olu�tu: " + e.getMessage());
		}
	}

	private void addErrMessage(String summary) {
		addMessage(summary, FacesMessage.SEVERITY_ERROR);
	}

	private void addMessage(String summary, Severity severity) {
		FacesMessage message = new FacesMessage(severity, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Authenticator getAuthenticator() {
		return authenticator;
	}

	public void setAuthenticator(Authenticator authenticator) {
		this.authenticator = authenticator;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
}
