package com.soapuiweb.portal.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Pattern;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.soapuiweb.core.Utils;

import com.soapuiweb.portal.model.Project;
import com.soapuiweb.portal.model.Projects;
import com.soapuiweb.portal.model.User;
import com.soapuiweb.portal.model.Users;

@ApplicationScoped
@ManagedBean(name = "authenticator")
public class Authenticator implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String rootFolder = "soapui-web";
	private static final String userFile = rootFolder + File.separatorChar + "users.xml";
	private static final String projectFolder = rootFolder;// rootFolder + File.separatorChar + "#userName#";
	private static final String projectFile = projectFolder + File.separatorChar + "#userName#.xml";

	private static final Object userLock = new Object();
	private static final Pattern userNamePattern = Pattern.compile("[_A-Za-z0-9-\\+]{6,}");

	static {
		new File(rootFolder).mkdirs();
	}

	public User login(String userName, String password) throws Exception {
		synchronized (userLock) {
			Users users = getUsers();
			for (User u : users.getUsers()) {
				if (u.getUserName().equals(userName) && u.getPassword().equals(password)) {
					return u;
				}
			}
			throw new Exception("Kullan�c� ad� yada �ifre hatal�.");
		}
	}

	private Users getUsers() throws Exception {
		File f = new File(userFile);
		if (f.exists()) {
			return (Users) unmarshal(f, Users.class);
		} else {
			Users u = new Users();
			marshal(f, u);
			return u;
		}
	}

	@SuppressWarnings("resource")
	private Object unmarshal(File f, Class<?> clazz) throws Exception {
		FileInputStream fis = new FileInputStream(f);
		if (fis.available() > 0) {
			JAXBContext jc = JAXBContext.newInstance(clazz);
			Unmarshaller u = jc.createUnmarshaller();
			return u.unmarshal(fis);
		} else {
			return clazz.newInstance();
		}
	}

	private void marshal(File f, Object o) throws Exception {
		JAXBContext jc = JAXBContext.newInstance(o.getClass());
		Marshaller m = jc.createMarshaller();
		m.marshal(o, f);
	}

	public Project getProject(User user, int id) throws Exception {
		synchronized (user.getUserName().intern()) {
			Projects projects = getProjects(user);
			for (Project p : projects.getProjects()) {
				if (p.getId() == id) {
					return p;
				}
			}
			return null;
		}
	}

	public Project deleteProject(User user, int id) throws Exception {
		synchronized (user.getUserName().intern()) {
			Projects projects = getProjects(user);
			for (Iterator<Project> its = projects.getProjects().iterator(); its.hasNext();) {
				Project p = its.next();
				if (p.getId() == id) {
					its.remove();
					marshal(getProjectFile(user), projects);
					return p;
				}
			}
		}
		return null;
	}

	private Projects getProjects(User user) throws Exception {
		synchronized (user.getUserName().intern()) {
			return (Projects) unmarshal(getProjectFile(user), Projects.class);
		}
	}

	public Projects getProjectsWithoutData(User user) throws Exception {
		synchronized (user.getUserName().intern()) {
			Projects ps = getProjects(user);
			for (Project p : ps.getProjects()) {
				p.setData(null);
			}
			return ps;
		}
	}

	private File getProjectFile(User user) throws Exception {
		File folder = new File(projectFolder.replace("#userName#", user.getUserName()));
		if (!folder.exists()) {
			folder.mkdirs();
		}
		File f = new File(projectFile.replace("#userName#", user.getUserName()));
		if (!f.exists()) {
			f.createNewFile();
			marshal(f, new Projects());
		}
		return f;
	}

	public void saveProject(User user, Project project) throws Exception {
		synchronized (user.getUserName().intern()) {
			Projects projects = getProjects(user);
			boolean f = false;
			int lastId = 1;
			for (Project p : projects.getProjects()) {
				if (p.getId() > lastId) {
					lastId = p.getId();
				}
				if (p.getId() == project.getId()) {
					p.setData(project.getData());
					p.setName(project.getName());
					f = true;
				}
			}
			if (!f) {
				project.setId(lastId + 1);
				projects.getProjects().add(project);
				marshal(getProjectFile(user), projects);
			} else {
				marshal(getProjectFile(user), projects);
			}

		}
	}

	public User createUser(String userName, String password1, String password2, String email) throws Exception {
		if (userName == null || userName.trim().length() < 6) {
			throw new Exception("Kullan�c� ad� en az 6 karakter olmal�d�r.");
		}
		if (!userNamePattern.matcher(userName).matches()) {
			throw new Exception("Kullan�c� ad� ge�ersiz.");
		}
		if (password1 == null || password1.trim().length() < 6) {
			throw new Exception("�ifre en az 6 karakter olmal�d�r.");
		}
		if (password2 == null || password2.trim().length() < 6) {
			throw new Exception("�ifre en az 6 karakter olmal�d�r.");
		}
		if (!password1.equals(password2)) {
			throw new Exception("�ifre do�rulama kodu ge�ersiz.");
		}
		if (email == null || email.trim().length() == 0) {
			throw new Exception("E-posta adresi girilmelidir.");
		}
		if (!Utils.isValidEmail(email)) {
			throw new Exception("Ge�erli bir e-posta adresi girilmelidir.");
		}
		synchronized (userLock) {
			Users users = getUsers();
			for (User u : users.getUsers()) {
				if (u.getUserName().equals(userName)) {
					throw new Exception("Kullan�c� ad� ge�ersiz.");
				}
			}
			User u = new User();
			u.setCreateDate(new Date());
			u.setUserName(userName);
			u.setPassword(password1);
			u.setEmail(email);
			users.getUsers().add(u);
			File f = new File(userFile);
			if (!f.exists()) {
				f.createNewFile();
			}
			JAXBContext jc = JAXBContext.newInstance(Users.class);
			Marshaller m = jc.createMarshaller();
			m.marshal(users, f);
			return u;
		}
	}
}
