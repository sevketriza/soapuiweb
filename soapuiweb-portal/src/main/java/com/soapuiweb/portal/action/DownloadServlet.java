package com.soapuiweb.portal.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.soapuiweb.core.Utils;

public class DownloadServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		byte[] b = new byte[1024];
		int len;
		resp.setContentType("application/zip");
		resp.setHeader("content-disposition", "inline; filename=\"data_" + Utils.formatDate(new Date(), "yyyyMMdd") + ".zip\"");
		ZipOutputStream zos = new ZipOutputStream(resp.getOutputStream());
		File file = new File(Authenticator.rootFolder);
		for (File f : file.listFiles()) {
			ZipEntry ze = new ZipEntry(f.getName());
			zos.putNextEntry(ze);
			FileInputStream fis = new FileInputStream(f);
			while ((len = fis.read(b)) != -1) {
				zos.write(b, 0, len);
			}
			fis.close();
			zos.closeEntry();
		}
		zos.close();
	}
}
