package com.soapuiweb.portal.action;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.soapuiweb.portal.model.User;

@ManagedBean(name = "userHome")
@ViewScoped
public class UserHome implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userName;
	private String password1;
	private String password2;
	private String email;

	@ManagedProperty(value = "#{authenticator}")
	private Authenticator authenticator;

	@ManagedProperty(value = "#{loginHome}")
	private LoginHome loginHome;

	public void create() {
		try {
			authenticator.createUser(userName, password1, password2, email);
			loginHome.setUserName(userName);
			loginHome.setPassword(password1);
			loginHome.login();
		} catch (Exception e) {
			addErrMessage(e.getMessage());
		}
	}

	private void addErrMessage(String summary) {
		addMessage(summary, FacesMessage.SEVERITY_ERROR);
	}

	private void addMessage(String summary, Severity severity) {
		FacesMessage message = new FacesMessage(severity, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Authenticator getAuthenticator() {
		return authenticator;
	}

	public void setAuthenticator(Authenticator authenticator) {
		this.authenticator = authenticator;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LoginHome getLoginHome() {
		return loginHome;
	}

	public void setLoginHome(LoginHome loginHome) {
		this.loginHome = loginHome;
	}

	public String getPassword1() {
		return password1;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}
}
