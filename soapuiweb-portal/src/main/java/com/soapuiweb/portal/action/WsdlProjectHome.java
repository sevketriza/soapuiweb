package com.soapuiweb.portal.action;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

import java.io.Serializable;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.transform.TransformerException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabCloseEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.soapuiweb.core.Utils;
import org.soapuiweb.core.WsRequestParser;
import org.soapuiweb.core.WsdlProjectCreator;
import org.soapuiweb.core.model.BindingWrapper;
import org.soapuiweb.core.model.ElementMeta;
import org.soapuiweb.core.model.OperationWrapper;
import org.soapuiweb.core.model.WsdlProject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.soapuiweb.portal.model.EnumTypes.NodeType;
import com.soapuiweb.portal.model.KeyValue;
import com.soapuiweb.portal.model.NodeElement;
import com.soapuiweb.portal.model.Project;
import com.soapuiweb.portal.model.WsdlProjectWrapper;

@ManagedBean
@SessionScoped
public class WsdlProjectHome implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Log logger = LogFactory.getLog(WsRequestParser.class);

	// private String wsdlUri = "https://www.sbm.org.tr/EgmSorguService-prod-V1.0/egmSorguService?wsdl";
	// private String wsdlUri = "https://www.sbm.org.tr/TrafikPoliceWS-prod-V2.0/TrafikPoliceIslemleriServisiSoapHttpPort?wsdl";
	// private String wsdlUri = "http://www.tarsim.org.tr/tarsimWS/services/ParameterWs?wsdl";
	// private String wsdlUri = "https://www.sbm.org.tr/ZMSSPoliceOnlineWS-prod-V2.0/ZMSSPoliceOnlineService?wsdl";
	private String wsdlUri;
	private DefaultTreeNode root;

	private List<NodeElement> requests = new ArrayList<NodeElement>();// Collections.synchronizedList(new ArrayList<NodeElement>());

	private int activeRequestTabIndex;

	private List<String> headers = new ArrayList<String>();

	private int activeProjectId = -1;

	@ManagedProperty(value = "#{loginHome}")
	private LoginHome loginHome;
	@ManagedProperty(value = "#{authenticator}")
	private Authenticator authenticator;

	{
		headers.add("Username");
		headers.add("KurumKod");
	}

	public void clean() {
		this.root = null;
		requests.clear();
		activeRequestTabIndex = 0;
		activeProjectId = -1;
	}

	public Locale getEnLocale() {
		return Locale.ENGLISH;
	}

	private void setTrusted() throws KeyManagementException, NoSuchAlgorithmException {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };
		// Install the all-trusting trust manager
		final SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	}

	public void load(WsdlProject wsdlProject) {
		root = new DefaultTreeNode();
		root.setExpanded(true);
		root.setSelectable(false);
		root.setData(wsdlProject.getName());
		for (BindingWrapper bw : wsdlProject.getBindings()) {
			NodeElement ne = new NodeElement();
			ne.setName(bw.getName());
			ne.setNodeType(NodeType.BINDING);
			ne.setLocation(bw.getLocation());
			ne.setSoapVersion(bw.getSoapVersion());
			TreeNode bindingNode = new DefaultTreeNode(ne, root);
			bindingNode.setExpanded(true);
			bindingNode.setSelectable(false);
			ne.setTreeNode(bindingNode);
			for (OperationWrapper ow : bw.getOperations()) {
				ne = new NodeElement();
				ne.setName(ow.getName());
				ne.setNodeType(NodeType.OPERATION);
				ne.setMeta(ow.getMeta());
				ne.setSoapAction(ow.getSoapAction());
				ne.setLocation(bw.getLocation());
				ne.setSoapVersion(bw.getSoapVersion());
				DefaultTreeNode opNode = new DefaultTreeNode(ne, bindingNode);
				opNode.setExpanded(true);
				opNode.setSelectable(false);
				ne.setTreeNode(opNode);
			}
		}
	}

	public boolean wsdlParse() {
		activeProjectId = -1;
		requests.clear();
		activeRequestTabIndex = 0;
		try {
			setTrusted();
			URLConnection con = new URL(wsdlUri).openConnection();
			load(WsdlProjectCreator.getInstance().createProject(con.getInputStream()));
			return true;
		} catch (Exception e) {
			addMessage("Hata: " + e.getMessage(), FacesMessage.SEVERITY_ERROR);
			logger.error("", e);
			return false;
		}
	}

	public void clearResponse() {
		requests.get(activeRequestTabIndex).setResponseXml(null);
	}

	public NodeElement getAktifRequest() {
		if (requests.size() > 0) {
			return requests.size() > activeRequestTabIndex ? requests.get(activeRequestTabIndex) : null;
		}
		return null;
	}

	public List<String> getHeaders() {
		return headers;
	}

	private String format(String unformattedXml) {
		try {
			return Utils.formatXml(unformattedXml);
		} catch (Exception e) {
			return unformattedXml;
		}
	}

	private NodeElement getActiveReq() {
		return requests.get(activeRequestTabIndex);
	}

	public void prepareXml(NodeElement reqNode) {
		if (reqNode.getActiveTab() == 0) {
			formToXml();
		} else if (reqNode.getActiveTab() == 1) {
			xmlToForm();
		}
	}

	public void onAppletCallEnd() {
		StringWriter sw = new StringWriter();
		sw.append("<pre class=\"pre\">");
		String xml = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("xml");
		try {
			Document document = parse(Utils.formatXml(xml));
			toHtmlFormat(document, sw);
		} catch (Exception e) {
			logger.debug("", e);
			sw.append(escapeHtml(xml));
		}
		sw.append("</pre>");
		requests.get(activeRequestTabIndex).setResponseXml(sw.toString());
		requests.get(activeRequestTabIndex).setActiveTab(2);
	}

	public void toHtmlFormat(Node root, StringWriter writer) {
		if (root.getNodeType() == Node.ELEMENT_NODE) {
			writer.append(toElementBracket("<"));
			writer.append(toElementName(root.getNodeName()));
		}
		if (root.getAttributes() != null && root.getAttributes().getLength() > 0) {
			writer.append(" ");

			for (int i = 0; i < root.getAttributes().getLength(); i++) {
				Node at = root.getAttributes().item(i);
				writer.append(toAttributeName(at.getNodeName()));
				writer.append(toAttributeEquals("="));
				writer.append(toAttributeValue("\"" + nullControlled(at.getNodeValue()) + "\""));

				if (i < root.getAttributes().getLength() - 1) {
					writer.append(" ");
				}
			}
		}
		if (root.getNodeType() == Node.ELEMENT_NODE) {
			writer.append(toElementBracket(">"));
		}
		if (root.getNodeValue() != null && root.getNodeValue().length() > 0) {
			writer.append(toElementValue(root.getNodeValue()));
		}
		if (root.getChildNodes() != null) {
			for (int i = 0; i < root.getChildNodes().getLength(); i++) {
				toHtmlFormat(root.getChildNodes().item(i), writer);

			}
		}
		if (root.getNodeType() == Node.ELEMENT_NODE) {
			writer.append(toElementBracket("</"));
			writer.append(toElementName(root.getNodeName()));
			writer.append(toElementBracket(">"));
		}
	}

	private String toElementName(String s) {
		return "<span class=\"elementName\">" + escapeHtml(s) + "</span>";
	}

	private String toElementValue(String s) {
		return "<span class=\"elementValue\">" + escapeHtml(s) + "</span>";
	}

	private String toElementBracket(String s) {
		return "<span class=\"elementBracket\">" + escapeHtml(s) + "</span>";
	}

	private String toAttributeName(String s) {
		return "<span class=\"attributeName\">" + escapeHtml(s) + "</span>";
	}

	private String toAttributeValue(String s) {
		return "<span class=\"attributeValue\">" + escapeHtml(s) + "</span>";
	}

	private String toAttributeEquals(String s) {
		return "<span class=\"attributeEquals\">" + escapeHtml(s) + "</span>";
	}

	private String nullControlled(String nodeValue) {
		return nodeValue == null ? "" : nodeValue;
	}

	private String getPath(Node node) {
		List<String> tags = new ArrayList<String>();
		do {
			if (isBodyTag(node)) {
				break;
			}
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				tags.add(getTagName(node) + "[" + getChildIndex(node) + "]");
			}
			node = node.getParentNode();
		} while (node != null);

		StringBuilder path = new StringBuilder();
		for (int i = tags.size() - 1; i >= 0; i--) {
			path.append(tags.get(i));
			if (i != 0) {
				path.append(".");
			}
		}

		return path.toString();
	}

	private int getChildIndex(Node node) {
		if (node.getParentNode() != null) {
			NodeList childNodes = node.getParentNode().getChildNodes();
			int c = 0;
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node item = childNodes.item(i);
				if (item == node) {
					return c;
				}
				if (item.getNodeName().equals(node.getNodeName())) {
					c += 1;
				}
			}
		}
		return 0;
	}

	private int getChildIndex(ElementMeta meta) {
		if (meta.getParent() != null) {
			int c = 0;
			for (ElementMeta ch : meta.getParent().getChilds()) {
				if (ch == meta) {
					return c;
				}
				if (ch.getName().equals(meta.getName())) {
					c += 1;
				}
			}
			return meta.getParent().getChilds().indexOf(meta);
		}
		return 0;
	}

	private String getPath(ElementMeta node) {
		List<String> tags = new ArrayList<String>();
		do {
			if (isBodyTag(node)) {
				break;
			}
			tags.add(getTagName(node) + "[" + getChildIndex(node) + "]");
			node = node.getParent();
		} while (node != null);

		StringBuilder path = new StringBuilder();
		for (int i = tags.size() - 1; i >= 0; i--) {
			path.append(tags.get(i));
			if (i != 0) {
				path.append(".");
			}
		}

		return path.toString();
	}

	private String getTagName(Node node) {
		int index = node.getNodeName().indexOf(":");
		return index == -1 ? node.getNodeName() : node.getNodeName().substring(index + 1);
	}

	private String getTagName(ElementMeta node) {
		int index = node.getName().indexOf(":");
		return index == -1 ? node.getName() : node.getName().substring(index + 1);
	}

	private boolean isBodyTag(Node node) {
		return getTagName(node).toLowerCase().equals("body");
	}

	private boolean isEnvelopeTag(Node node) {
		return getTagName(node).toLowerCase().equals("envelope");
	}

	private boolean isBodyTag(ElementMeta node) {
		return getTagName(node).toLowerCase().equals("body");
	}

	private void formToXml() {
		NodeElement reqNode = requests.get(activeRequestTabIndex);
		if (reqNode.getXml() != null && reqNode.getXml().length() > 0) {
			try {
				Document xmlDoc = parse(reqNode.getXml());
				List<ElementMeta> values = new ArrayList<ElementMeta>();
				listValueTypeNodes(reqNode.getMeta(), values);
				for (ElementMeta vt : values) {
					NodeList nodes = xmlDoc.getElementsByTagName(vt.getName());
					if (nodes != null && nodes.getLength() > 0) {
						String path = getPath(vt);
						for (int i = 0; i < nodes.getLength(); i++) {
							Node item = nodes.item(i);
							if (item != null) {
								String itemPath = getPath(item);
								if (itemPath.equals(path)) {
									item.setTextContent(vt.getValue());
								}
							}
						}
					}
				}
				reqNode.setXml(toXml(xmlDoc));
			} catch (Exception e) {
				addMessage("XML format� hatal�: " + e.getMessage());
				logger.warn("", e);
			}

		} else {
			reqNode.setXml(format(reqNode.getMeta().toXml()));
		}
	}

	private void xmlToForm() {
		NodeElement reqNode = requests.get(activeRequestTabIndex);
		if (reqNode.getXml() != null && reqNode.getXml().length() > 0) {
			try {
				Document xmlDoc = parse(reqNode.getXml());
				List<ElementMeta> values = new ArrayList<ElementMeta>();
				listValueTypeNodes(reqNode.getMeta(), values);
				for (ElementMeta vt : values) {
					NodeList nodes = xmlDoc.getElementsByTagName(vt.getName());
					if (nodes != null && nodes.getLength() > 0) {
						String path = getPath(vt);
						for (int i = 0; i < nodes.getLength(); i++) {
							Node item = nodes.item(i);
							if (item != null) {
								String itemPath = getPath(item);
								if (itemPath.equals(path)) {
									vt.setValue(item.getTextContent());
								}
							}
						}
					}
				}
				reqNode.setXml(toXml(xmlDoc));
			} catch (Exception e) {
				addMessage("XML format� hatal�: " + e.getMessage());
				logger.warn("", e);
			}

		}

	}

	private void addMessage(String summary) {
		addMessage(summary, FacesMessage.SEVERITY_INFO);
	}

	private void addError(String summary) {
		addMessage(summary, FacesMessage.SEVERITY_ERROR);
	}

	private void addMessage(String summary, Severity severity) {
		FacesMessage message = new FacesMessage(severity, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	private void listValueTypeNodes(ElementMeta root, List<ElementMeta> values) {
		if (root.getDataType() != null) {
			values.add(root);
		}
		if (root.getChilds() != null) {
			for (ElementMeta c : root.getChilds()) {
				listValueTypeNodes(c, values);
			}
		}
	}

	private String toXml(Document doc) throws TransformerException {
		return Utils.formatXml(doc);
	}

	private Document parse(String xml) throws Exception {
		return Utils.toDocument(xml);
	}

	private void toTreeNode(TreeNode treeNode, ElementMeta meta) {
		TreeNode newNode = new DefaultTreeNode(meta, treeNode);
		newNode.setExpanded(true);
		if (meta.getChilds() != null) {
			for (ElementMeta ch : meta.getChilds()) {
				toTreeNode(newNode, ch);
			}
		}
	}

	public void onFormTabChanged() {
		int newTabIndex = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("newTabIndex"));
		if (newTabIndex == 0) {
			xmlToForm();
		} else if (newTabIndex == 1) {
			formToXml();
		} else if (newTabIndex == 2) {
			if (getActiveReq().getActiveTab() == 0) {
				formToXml();
			} else if (getActiveReq().getActiveTab() == 1) {
				xmlToForm();
			}
		}
	}

	public void onRequestTabClosed(TabCloseEvent event) {
		int closedIndex = -1;
		for (int i = 0; i < requests.size(); i++) {
			if (requests.get(i) == event.getData()) {
				closedIndex = i;
				break;
			}
		}
		for (Iterator<NodeElement> iterator = requests.iterator(); iterator.hasNext();) {
			NodeElement req = (NodeElement) iterator.next();
			if (req == event.getData()) {
				if (closedIndex <= getActiveRequestTabIndex()) {
					setActiveRequestTabIndex(getActiveRequestTabIndex() - 1);
				}
				iterator.remove();
				break;
			}
		}
	}

	public void onLeftTreeNodeSelect(NodeSelectEvent event) {
		NodeElement ne = (NodeElement) event.getTreeNode().getData();
		if (ne.getNodeType() == NodeType.REQUEST && !requests.contains(ne)) {
			requests.add(ne);
		}
		for (int i = 0; i < requests.size(); i++) {
			if (requests.get(i) == ne) {
				setActiveRequestTabIndex(i);
				break;
			}
		}
	}

	public void activeRequest() {
		requests.remove(activeRequestTabIndex);
		if (activeRequestTabIndex > requests.size() - 1) {
			setActiveRequestTabIndex(requests.size() - 1);
		}
	}

	public void closeAllRequests() {
		requests.clear();
		setActiveRequestTabIndex(-1);
	}

	public void closeOtherRequests() {
		NodeElement temp = requests.get(activeRequestTabIndex);
		requests.clear();
		requests.add(temp);
		setActiveRequestTabIndex(0);
	}

	public void deleteRequest(NodeElement operationNode) {
		TreeNode metaRoot = operationNode.getTreeNode();
		metaRoot.getParent().getChildren().remove(metaRoot);

		int ri = requests.indexOf(operationNode);
		if (ri != -1) {
			requests.remove(ri);
		}
		if (activeRequestTabIndex == ri) {
			setActiveRequestTabIndex(requests.size() - 1);
		} else {
			if (activeRequestTabIndex > requests.size() - 1) {
				setActiveRequestTabIndex(requests.size() - 1);
			}
		}
	}

	public void addRequest(NodeElement operationNode) {
		NodeElement neReq = new NodeElement();
		neReq.setName(operationNode.getName() + " " + (operationNode.getTreeNode().getChildCount() + 1));
		neReq.setNodeType(NodeType.REQUEST);
		neReq.setMeta(operationNode.getMeta().clone());
		neReq.setLocation(operationNode.getLocation());
		neReq.setSoapAction(operationNode.getSoapAction());
		neReq.setSoapVersion(operationNode.getSoapVersion());
		DefaultTreeNode req = new DefaultTreeNode(neReq, operationNode.getTreeNode());
		neReq.setTreeNode(req);
		requests.add(neReq);
		TreeNode root = new DefaultTreeNode();
		root.setExpanded(true);
		toTreeNode(root, neReq.getMeta());
		neReq.setMetaRoot(root);
		setActiveRequestTabIndex(requests.size() - 1);
		debug(operationNode.getMeta());
		debug(neReq.getMeta());
	}

	public void cloneNode(ElementMeta metaRoot) {
		ElementMeta clonedMeta = metaRoot.clone();
		if (metaRoot.getNext() != null) {
			metaRoot.getParent().insertBefore(clonedMeta, metaRoot.getNext());
		} else {
			metaRoot.getParent().appendChild(clonedMeta);
		}

		NodeElement neReq = requests.get(activeRequestTabIndex);
		DefaultTreeNode req = new DefaultTreeNode(neReq, neReq.getTreeNode());
		neReq.setTreeNode(req);
		TreeNode root = new DefaultTreeNode();
		root.setExpanded(true);
		toTreeNode(root, neReq.getMeta());
		neReq.setMetaRoot(root);

		if (neReq.getXml() != null && neReq.getXml().length() > 0) {
			try {
				Document xmlDoc = parse(neReq.getXml());
				NodeList nodes = xmlDoc.getElementsByTagName(metaRoot.getName());
				if (nodes != null && nodes.getLength() > 0) {
					String path = getPath(metaRoot);
					int len = nodes.getLength();
					for (int i = 0; i < len; i++) {
						Node item = nodes.item(i);
						if (item != null) {
							String itemPath = getPath(item);
							if (itemPath.equals(path)) {
								Node cloneNode = item.cloneNode(true);
								if (item.getParentNode() != null) {
									Node nextElement = nextElement(item);
									if (nextElement != null) {
										item.getParentNode().insertBefore(cloneNode, nextElement);
									} else {
										item.getParentNode().appendChild(cloneNode);
									}
								} else {
									xmlDoc.appendChild(cloneNode);
								}
							}
						}
					}
				}
				neReq.setXml(toXml(xmlDoc));
			} catch (Exception e) {
				addMessage("XML format� hatal�: " + e.getMessage());
				logger.warn("", e);
			}
		}
	}

	public void deleteNode(ElementMeta metaRoot) {
		NodeElement neReq = requests.get(activeRequestTabIndex);
		if (neReq.getXml() != null && neReq.getXml().length() > 0) {
			try {
				Document xmlDoc = parse(neReq.getXml());
				NodeList nodes = xmlDoc.getElementsByTagName(metaRoot.getName());
				if (nodes != null && nodes.getLength() > 0) {
					String path = getPath(metaRoot);
					int len = nodes.getLength();
					for (int i = 0; i < len; i++) {
						Node item = nodes.item(i);
						if (item != null) {
							String itemPath = getPath(item);
							if (itemPath.equals(path)) {
								if (item.getParentNode() != null) {
									item.getParentNode().removeChild(item);
								} else {
									xmlDoc.removeChild(item);
								}
							}
						}
					}
				}
				neReq.setXml(toXml(xmlDoc));
			} catch (Exception e) {
				addMessage("XML format� hatal�: " + e.getMessage());
				logger.warn("", e);
			}
		}
		if (metaRoot.getParent() != null) {
			metaRoot.getParent().removeChild(metaRoot);
		}
		DefaultTreeNode req = new DefaultTreeNode(neReq, neReq.getTreeNode());
		neReq.setTreeNode(req);
		TreeNode root = new DefaultTreeNode();
		root.setExpanded(true);
		toTreeNode(root, neReq.getMeta());
		neReq.setMetaRoot(root);
	}

	public void addHeader() {
		NodeElement reqNode = requests.get(activeRequestTabIndex);
		prepareXml(reqNode);
		if (reqNode.getXml() != null && reqNode.getXml().length() > 0) {
			try {
				Document xmlDoc = parse(reqNode.getXml());
				NodeList childNodes = xmlDoc.getChildNodes();
				if (childNodes != null && childNodes.getLength() > 0) {
					Node body = null, envelope = null;
					for (int i = 0; i < childNodes.getLength(); i++) {
						Node item = childNodes.item(i);
						if (isEnvelopeTag(item)) {
							envelope = item;
							childNodes = item.getChildNodes();
							for (i = 0; i < childNodes.getLength(); i++) {
								item = childNodes.item(i);
								if (isBodyTag(item)) {
									body = item;
									break;
								}
							}
							break;
						}
					}
					if (envelope != null) {
						String prefix = "";
						int ind = envelope.getNodeName().indexOf(":");
						if (ind != -1) {
							prefix = envelope.getNodeName().substring(0, ind);
						}
						Element header = xmlDoc.createElement(prefix + ":Header");
						Element sec = xmlDoc.createElement("Security");
						sec.setAttribute("xmlns", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
						for (KeyValue kv : reqNode.getSecurityHeaders()) {
							if (kv.getKey() != null && kv.getKey().length() > 0) {
								Element e = xmlDoc.createElement(kv.getKey());
								e.setTextContent(kv.getValue());
								sec.appendChild(e);
							}
						}
						header.appendChild(sec);
						if (body != null) {
							envelope.insertBefore(header, body);
						} else {
							envelope.appendChild(header);
						}
						reqNode.setXml(toXml(xmlDoc));
					}
				}

			} catch (Exception e) {
				logger.error("", e);
			}
		}

	}

	public void addHeaderRow() {
		NodeElement reqNode = requests.get(activeRequestTabIndex);
		reqNode.getSecurityHeaders().add(new KeyValue("", ""));
	}

	public void removeHeaderRow(KeyValue kv) {
		NodeElement reqNode = requests.get(activeRequestTabIndex);
		reqNode.getSecurityHeaders().remove(reqNode.getSecurityHeaders().indexOf(kv));
	}

	private Node nextElement(Node node) {
		Node next = node.getNextSibling();
		while (next != null) {
			if (next.getNodeType() == Node.ELEMENT_NODE) {
				return next;
			}
			next = next.getNextSibling();
		}
		return null;
	}

	private void debug(ElementMeta mc) {
		if (logger.isDebugEnabled()) {
			logger.debug(mc.toString() + " name:" + mc.getName() + " value:" + mc.getValue());
			List<ElementMeta> childNodes = mc.getChilds();
			if (childNodes != null && childNodes.size() > 0) {
				for (int i = 0; i < childNodes.size(); i++) {
					ElementMeta item = childNodes.get(i);
					debug(item);
				}
			}
		}
	}

	public void saveProject() throws Exception {
		try {
			WsdlProjectWrapper pw = new WsdlProjectWrapper();
			pw.setRequests(requests);
			pw.setWsdl(wsdlUri);
			pw.setActiveRequestTabIndex(activeRequestTabIndex);
			pw.setRoot(root);

			Project p = new Project();
			p.setName((String) root.getData());
			p.setData(Utils.serialize(pw));
			p.setId(activeProjectId);
			authenticator.saveProject(loginHome.getUser(), p);
			addMessage("Proje kaydedildi");
			activeProjectId = p.getId();

			loginHome.loadProjects();

		} catch (Exception e) {
			logger.error("", e);
			addError("Bekleyen bir hata olu�tu: " + e.getMessage());
		}
	}

	public void loadProject(int id) throws Exception {
		try {
			Project project = authenticator.getProject(loginHome.getUser(), id);
			WsdlProjectWrapper pw = (WsdlProjectWrapper) Utils.deserialize(project.getData());
			this.root = pw.getRoot();
			this.requests = pw.getRequests();
			this.activeRequestTabIndex = pw.getActiveRequestTabIndex();
			this.wsdlUri = pw.getWsdl();
			this.activeProjectId = project.getId();
		} catch (Exception e) {
			logger.error("", e);
			addError("Bekleyen bir hata olu�tu: " + e.getMessage());
		}
	}

	public void deleteProject(int id) throws Exception {
		try {
			Project deletedProject = authenticator.deleteProject(loginHome.getUser(), id);
			if (deletedProject != null) {
				if (deletedProject.getId() == this.activeProjectId) {
					this.root = null;
					this.requests.clear();
					this.activeRequestTabIndex = 0;
					this.wsdlUri = null;
					this.activeProjectId = -1;
				}
				addMessage("Proje silindi");
				loginHome.loadProjects();
			}
		} catch (Exception e) {
			logger.error("", e);
			addError("Bekleyen bir hata olu�tu: " + e.getMessage());
		}
	}

	public void reloadProject(int id) throws Exception {
		Project project = authenticator.getProject(loginHome.getUser(), id);
		WsdlProjectWrapper pw = (WsdlProjectWrapper) Utils.deserialize(project.getData());
		this.wsdlUri = pw.getWsdl();
		if (wsdlParse()) {
			root.setData(project.getName());
			this.activeProjectId = project.getId();
		}
	}

	public String getWsdlUri() {
		return wsdlUri;
	}

	public void setWsdlUri(String wsdlUri) {
		this.wsdlUri = wsdlUri;
	}

	public DefaultTreeNode getRoot() {
		return root;
	}

	public void setRoot(DefaultTreeNode root) {
		this.root = root;
	}

	public List<NodeElement> getRequests() {
		return requests;
	}

	public void setRequests(List<NodeElement> requests) {
		this.requests = requests;
	}

	public int getActiveRequestTabIndex() {
		return activeRequestTabIndex;
	}

	public void setActiveRequestTabIndex(int activeRequestTabIndex) {
		this.activeRequestTabIndex = activeRequestTabIndex;
	}

	public void onDateTimeSelect(SelectEvent event) {
		String val = (String) event.getObject();
		if (val != null) {
			HtmlInputText input = (HtmlInputText) event.getSource();
			input.setValue(val.replaceAll(" ", ""));
		}
	}

	public LoginHome getLoginHome() {
		return loginHome;
	}

	public void setLoginHome(LoginHome loginHome) {
		this.loginHome = loginHome;
	}

	public Authenticator getAuthenticator() {
		return authenticator;
	}

	public void setAuthenticator(Authenticator authenticator) {
		this.authenticator = authenticator;
	}

	public int getActiveProjectId() {
		return activeProjectId;
	}

	public void setActiveProjectId(int activeProjectId) {
		this.activeProjectId = activeProjectId;
	}

	public void logout() {
		loginHome.setUser(null);
		loginHome.setProjects(null);
		clean();
		wsdlUri = null;
	}
}
