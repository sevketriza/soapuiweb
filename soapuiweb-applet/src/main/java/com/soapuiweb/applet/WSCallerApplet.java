package com.soapuiweb.applet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.JApplet;

import netscape.javascript.JSObject;

public class WSCallerApplet extends JApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String uri;
	private String requestXml;
	private String soapAction;
	private String soapVersion;
	
	private SecurityManager sm;

	public void test(String s) {
		System.out.println(s);
		JSObject window = JSObject.getWindow(this);
		window.eval("onAppletCallEnd();");
	}

	public void invoke(String uri, String requestXml, String soapAction, String soapVersion) {
		this.uri = nullControl(uri);
		this.requestXml = nullControl(requestXml);
		this.soapAction = nullControl(soapAction);
		this.soapVersion = nullControl(soapVersion);

		JSObject window = JSObject.getWindow(this);
		try {
			System.out.println("giden: " + requestXml);
			String xml = invoke();
			System.out.println("gelen: " + xml);
			String jsType = escapeJavaScript(xml);
			window.eval("onAppletCallEnd([{name:'xml', value:'" + jsType + "'}]);");
		} catch (Exception e) {
			String err = escapeJavaScript(stackTrace(e));
			window.eval("onAppletCallEnd([{name:'xml', value:'" + err + "'}]);");
			e.printStackTrace();
		}
	}
	
	private SecurityManager sm() {
		if(sm == null) {
			sm = new CustomSecurityManager();
		}
		return sm;
	}

	private void setSecure() {
		System.setSecurityManager(sm());
	}
	
	private void setTrusted() throws KeyManagementException, NoSuchAlgorithmException {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };
        // Install the all-trusting trust manager
        final SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	}

	public void start() {
		setSecure();
		try {
			setTrusted();
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void startx() {
		uri = nullControl(getParameter("uri"));
		requestXml = nullControl(getParameter("xml"));
		soapAction = nullControl(getParameter("soapAction"));
		soapVersion = nullControl(getParameter("soapVersion"));
		// String activeIndex = nullControl(getParameter("activeIndex"));

		JSObject window = JSObject.getWindow(this);
		try {
			System.out.println("giden: " + requestXml);
			String xml = invoke();
			System.out.println("gelen: " + xml);
			String jsType = escapeJavaScript(xml);
			window.eval("onAppletCallEnd([{name:'xml', value:'" + jsType + "'}]);");
		} catch (Exception e) {
			String err = escapeJavaScript(stackTrace(e));
			window.eval("onAppletCallEnd([{name:'xml', value:'" + err + "'}]);");
			e.printStackTrace();
		}
	}

	private String stackTrace(Throwable t) {
		while (t.getCause() != null && t.getCause() != t) {
			t = t.getCause();
		}
		StringWriter sw = new StringWriter();
		t.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

	private String nullControl(String s) {
		return s == null ? "" : s;
	}

	private static String hex(char ch) {
		return Integer.toHexString(ch).toUpperCase(Locale.ENGLISH);
	}

	private String escapeJavaScript(String str) {
		if (str == null) {
			return null;
		}
		StringWriter out = new StringWriter(str.length() * 2);
		int sz;
		sz = str.length();
		for (int i = 0; i < sz; i++) {
			char ch = str.charAt(i);

			// handle unicode
			if (ch > 0xfff) {
				out.write("\\u" + hex(ch));
			} else if (ch > 0xff) {
				out.write("\\u0" + hex(ch));
			} else if (ch > 0x7f) {
				out.write("\\u00" + hex(ch));
			} else if (ch < 32) {
				switch (ch) {
				case '\b':
					out.write('\\');
					out.write('b');
					break;
				case '\n':
					out.write('\\');
					out.write('n');
					break;
				case '\t':
					out.write('\\');
					out.write('t');
					break;
				case '\f':
					out.write('\\');
					out.write('f');
					break;
				case '\r':
					out.write('\\');
					out.write('r');
					break;
				default:
					if (ch > 0xf) {
						out.write("\\u00" + hex(ch));
					} else {
						out.write("\\u000" + hex(ch));
					}
					break;
				}
			} else {
				switch (ch) {
				case '\'':
					out.write('\\');
					out.write('\'');
					break;
				case '"':
					out.write('\\');
					out.write('"');
					break;
				case '\\':
					out.write('\\');
					out.write('\\');
					break;
				case '/':
					out.write('\\');
					out.write('/');
					break;
				default:
					out.write(ch);
					break;
				}
			}
		}
		return out.toString();
	}

	private String invoke() throws MalformedURLException, IOException {
		HttpURLConnection con = null;
		InputStream in = null;
		try {
			con = (HttpURLConnection) new URL(uri).openConnection();
			con.setConnectTimeout(10000);
			con.setReadTimeout(30000);
			con.setDoOutput(true);

			System.out.println("soapVersion:" + soapVersion + ", soapAction:" + soapAction);

			con.setRequestMethod("POST");
			con.setUseCaches(false);
			if (soapVersion.equals("SOAP12")) {
				con.addRequestProperty("Content-Type", "application/soap+xml;charset=UTF-8;action=" + soapAction);
			} else if (soapVersion.equals("SOAP11")) {
				con.addRequestProperty("Content-Type", "text/xml;charset=UTF-8");
				con.addRequestProperty("SOAPAction", soapAction);
			}

			con.getOutputStream().write(requestXml.getBytes());

			System.out.println(con.getResponseCode() + " - " + con.getResponseMessage());

			if (con.getResponseCode() != 200) {
				in = con.getErrorStream();
			} else {
				in = con.getInputStream();
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			return sb.toString();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
				}
			}
			if (con != null) {
				try {
					con.disconnect();
				} catch (Exception e) {
				}
			}
		}
	}

}
